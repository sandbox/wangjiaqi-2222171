<?php

/**
 *
 */
function role_simple_translate_list(){
  global $language;
  $translated_roles = get_role_translate($language->language);
  $default_language = language_default();
  $default = $default_language->language;
  drupal_add_css(drupal_get_path('module', 'locale') . '/locale.css');
  $output = '';
  $roles = user_roles();
  $header = array(t('Role'), t('Language'), t('Edit'));
  $rows = array();
  foreach ($roles as $rid => $value) {
    $rows[$rid]['name'] = $value;
    $classs = array_key_exists($rid, $translated_roles)?'':' class="locale-untranslated"';
    $rows[$rid]['language'] = "<em{$classs}>" . $language->language . '</em>';
    if(($rid == DRUPAL_ANONYMOUS_RID || $rid == DRUPAL_AUTHENTICATED_RID )){
      $rows[$rid]['edit'] = t('translate');
    } else {
      $rows[$rid]['edit'] = l(t('translate'), "admin/config/regional/translate/roles/{$rid}");
    }
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}

/**
 *
 */
function role_simple_translate_form($form, &$form_state, $rid){
  global $language;
  $roles = user_roles();
  $form = array();
  $translated_roles = get_role_translate($language->language);
  $form['role'] = array(
    '#type' => 'item',
    '#markup' => $roles[$rid],
    '#title' => 'Role name'
  );
  $form['translate'] = array(
    '#type' => 'textfield',
    '#title' => $language->name,
    '#default_value' => isset($translated_roles[$rid]['translation'])?$translated_roles[$rid]['translation']:''
  );
  $form['rid'] = array(
    '#type' => 'hidden',
    '#value' => $rid
  );
  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => isset($translated_roles[$rid]['translation'])?$translated_roles[$rid]['translation']:''
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'save'
  );
  return $form;
}

/**
 *
 */
function role_simple_translate_form_submit($form, &$form_state){
  global $language;
  if(empty($form_state['values']['name'])){
    db_insert('role_translate')->fields(array('rid'=>$form_state['values']['rid'], 'translation'=>$form_state['values']['translate'], 'language'=>$language->language))->execute();
  } else {
    db_update('role_translate')->fields(array('translation'=>$form_state['values']['translate']))->condition('rid', $form_state['values']['rid'])->condition('language', $language->language)->execute();
  }
}

/**
 *
 */
function get_role_translate($lang= NULL){
  $query = db_select('role_translate', 'rt');
  $query->fields('rt');
  if(isset($lang)){
    $query->condition('language', $lang, '=');
  }
  $result = $query->execute();
  $roles = array();
  foreach ($result as $role) {
    $roles[$role->rid] = (array)$role;
  }
  return $roles;
}
